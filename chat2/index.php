<?php
	session_start();
	include("dados_conexao.php");

	if ($_POST)
	{
		try
		{   // tenta fazer a conexão e executar o INSERT
			$conecta = new PDO("mysql:host=$servidor;dbname=$banco", $usuario , $senha);
			$comandoSQL = "INSERT INTO tb_usuarios (nick) VALUES ('$_POST[nick]');";
			$grava = $conecta->prepare($comandoSQL); //testa o comando SQL
			$grava->execute(array());
			$_SESSION['nick_sala'] = $_POST['nick'];
			$_SESSION['cor_nick'] = $_POST['cor'];
			header("Location: sala_bate_papo.php"); //redirecionamento
		} catch(PDOException $e) { // casso retorne erro
			echo('Deu erro: ' . $e->getMessage());
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
  <title>SuperChat</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
</head>
<body style="font-family:Open Sans;">

	<div class="container">

	<br>
	<div class="well" align="center"><h1><strong>Bate-Papo Chat <strong>Projeto PHP</strong></h1></div>

	<form method="post">
	<div class="panel panel-success">
	<div class="panel-heading">Digite seu nick</div>
	<div class="panel-body">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<input type="text" name="nick" class="form-control" size="10" required>	<br/>

			Escolha um cor:
			<input type="color" name="cor">

			<br><br>

			<div align="center">
				<button type="submit" class="btn btn-success"> Entrar <span class="glyphicon glyphicon-log-in"></span> </button>
			</div>
		</div>
		<div class="col-md-4"></div>
	</div>
	</div>
	</form>
	</div>
</body>
</html>
